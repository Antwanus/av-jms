package com.antoonvereecken.avjms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HelloWorldMsg implements Serializable {

    static final long serialVersionUID = 698050163017229525L;

    private UUID msgId;
    private String msg;

}
