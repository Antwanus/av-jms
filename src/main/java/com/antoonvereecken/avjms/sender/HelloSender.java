package com.antoonvereecken.avjms.sender;

import com.antoonvereecken.avjms.config.JmsConfig;
import com.antoonvereecken.avjms.model.HelloWorldMsg;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class HelloSender {

    private final JmsTemplate jmsTemplate;
    private final ObjectMapper objectMapper;

    @Scheduled(fixedRate = 5000)
    public void sendMessage(){
        System.out.println("sendMessage() --> SENDING A MSG");

        HelloWorldMsg msg = HelloWorldMsg.builder()
                .msgId(UUID.randomUUID())
                .msg("Hello World")
                .build();

        jmsTemplate.convertAndSend(JmsConfig.MY_QUEUE, msg);

        System.out.println("Msg sent!");
    }

    @Scheduled(fixedRate = 5000)
    public void sendAndReceiveMessage() throws JMSException {

        HelloWorldMsg message = HelloWorldMsg
                .builder()
                .msgId(UUID.randomUUID())
                .msg("Hello")
                .build();

        Message receivedMsg = jmsTemplate.sendAndReceive(JmsConfig.MY_SEND_RCV_QUEUE, new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message helloMessage = null;

                try {
                    helloMessage = session.createTextMessage(objectMapper.writeValueAsString(message));
                    helloMessage.setStringProperty("_type", "com.antoonvereecken.avjms.model.HelloWorldMsg");

                    System.out.println("Sending Hello");

                    return helloMessage;

                } catch (JsonProcessingException e) {
                    throw new JMSException("boom");
                }
            }
        });

        System.out.println(receivedMsg.getBody(String.class));
    }

}
