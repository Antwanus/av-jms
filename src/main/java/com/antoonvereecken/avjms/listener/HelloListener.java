package com.antoonvereecken.avjms.listener;

import com.antoonvereecken.avjms.config.JmsConfig;
import com.antoonvereecken.avjms.model.HelloWorldMsg;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.UUID;

@RequiredArgsConstructor
//@Component
public class HelloListener {

    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.MY_QUEUE)
    public void listen(@Payload HelloWorldMsg helloWorldMessage,
                       @Headers MessageHeaders headers,
                       Message message
    ){

        System.out.println("listen() RECIEVED A MSG!");

        System.out.println(helloWorldMessage);


        // uncomment and view to see retry count in debugger
        // throw new RuntimeException("foo");

    }

    @JmsListener(destination = JmsConfig.MY_SEND_RCV_QUEUE)
    public void listenForHello(@Payload HelloWorldMsg helloWorldMessage,
                               @Headers MessageHeaders headers,
                               Message message
    ) throws JMSException {
        System.out.println("listenForHello() RECIEVED A MSG, SENDING R");
        HelloWorldMsg payloadMsg = HelloWorldMsg
                .builder()
                .msgId(UUID.randomUUID())
                .msg("<> WUUURLD <>")
                .build();
        jmsTemplate.convertAndSend(message.getJMSReplyTo (), payloadMsg);

        // uncomment and view to see retry count in debugger
        // throw new RuntimeException("foo");

    }

}
